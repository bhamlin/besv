# Bell Encoded Separated Values

An alternative to traditional printed-character separated value data storage.

## Usage

```rust
fn main() {
    let fh = fs::File::open("filename.besv").unwrap();
    let mut besv_reader = BesvReader::new(fh);

    for line in besv_reader.rows() {
        println!("{:?}", line);
    }
}
```

## Examples
* `create_sample_data`: Builds a sample data file for use with `besv2csv`.
* `besv2csv`: Reads a provided input besv file and outputs in comma separated format.