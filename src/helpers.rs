#![allow(unused)]

use crate::error::BesvError;
use error_stack::ResultExt;

pub fn bytes_to_str(input: &[u8]) -> error_stack::Result<&str, BesvError> {
    std::str::from_utf8(input).change_context_lazy(|| BesvError::Utf8ParseError)
}

pub fn bytes_to_string(input: &[u8]) -> error_stack::Result<String, BesvError> {
    bytes_to_str(input).map(|str| str.to_string())
}
