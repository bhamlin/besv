#![allow(unused)]

pub mod error;
pub mod helpers;

use error::BesvError;
use error_stack::ResultExt;
use helpers::bytes_to_str;
use itertools::Itertools;
use std::{
    borrow::Borrow,
    fmt::Display,
    io::{self, BufRead, BufReader, BufWriter, Read, Write},
    str::{from_utf8, Lines},
};

pub const BEL: u8 = 0x07;

#[cfg(windows)]
const LINE_ENDING: &str = "\r\n";
#[cfg(not(windows))]
const LINE_ENDING: &str = "\n";

pub struct BesvReader<R: ?Sized> {
    buf_reader: BufReader<R>,
}

impl<R: Read> BesvReader<R> {
    pub fn new(readable: R) -> BesvReader<R> {
        BesvReader {
            buf_reader: BufReader::new(readable),
        }
    }

    pub fn read_line(&mut self) -> error_stack::Result<Vec<String>, BesvError> {
        let mut buffer = String::new();
        let length = self
            .buf_reader
            .read_line(&mut buffer)
            .change_context_lazy(|| BesvError::IoError)?;
        let parts = buffer.trim().split(bytes_to_str(&[BEL])?);
        let strings = parts.map(|str| str.to_string()).collect_vec();

        Ok(strings)
    }

    pub fn rows(&mut self) -> BesvRowIter<R> {
        BesvRowIter { reader: self }
    }
}

pub struct BesvRowIter<'a, R: Read> {
    reader: &'a mut BesvReader<R>,
}

impl<R: Read> Iterator for BesvRowIter<'_, R> {
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Self::Item> {
        let line = self.reader.read_line().ok();

        let o_vec = line.as_ref();
        match o_vec {
            None => None,
            Some(vec) => {
                if vec.is_empty() {
                    None
                } else {
                    let first = vec.first();
                    if first.unwrap().is_empty() {
                        None
                    } else {
                        line
                    }
                }
            }
        }
    }
}

pub struct BesvWriter<W: ?Sized + Write> {
    buf_writer: BufWriter<W>,
}

impl<W: Write> BesvWriter<W> {
    pub fn new(writable: W) -> BesvWriter<W> {
        BesvWriter {
            buf_writer: BufWriter::new(writable),
        }
    }

    pub fn write_row<T: Display + ToString>(&mut self, row_values: &[T]) {
        let bell = from_utf8(&[BEL]).unwrap();
        let line = row_values.iter().join(bell);
        self.buf_writer.write_all(line.as_bytes());
        self.buf_writer.write_all(LINE_ENDING.as_bytes());
    }
}
