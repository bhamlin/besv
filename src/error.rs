use error_stack::Context;
use std::fmt::Display;

#[derive(Debug)]
pub enum BesvError {
    IoError,
    Utf8ParseError,
}

impl Context for BesvError {}
impl Display for BesvError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            BesvError::IoError => "Problem during i/o operation",
            BesvError::Utf8ParseError => "Problem turning bytes into a string",
        };

        write!(f, "{}", message)
    }
}
