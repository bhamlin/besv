use besv::{error::BesvError, BesvWriter, BEL};
use clap::Parser;
use error_stack::ResultExt;
use itertools::Itertools;
use std::{fmt::Display, fs, io::Write, str::from_utf8};

static NUMBER_WORDS: &[&str] = &[
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
    "eleven", "twelve", "thirteen", "fifteen",
];

type EmptyResult<E> = error_stack::Result<(), E>;

fn main() -> EmptyResult<BesvError> {
    let opts = Opts::parse();

    let mut besv_writer = match opts.output_path {
        Some(path) => Some(BesvWriter::new(
            fs::File::create(path).change_context_lazy(|| BesvError::IoError)?,
        )),
        None => None,
    };

    let row_headers = (0..opts.column_count)
        .map(|_| rand::random::<u8>())
        .map(number_to_name)
        .map(|name| format!("column {name}"))
        .collect_vec();
    output(besv_writer.as_mut(), row_headers);
    for _ in 0..opts.row_count {
        let row_values = (0..opts.column_count)
            .map(|_| rand::random::<u8>())
            .map(number_to_name)
            .collect_vec();
        output(besv_writer.as_mut(), row_values);
    }

    Ok(())
}

fn output<W, T>(writer: Option<&mut BesvWriter<W>>, content: Vec<T>)
where
    W: Write,
    T: Display + ToString,
{
    match writer {
        None => println!("{}", content.iter().join(from_utf8(&[BEL]).unwrap())),
        Some(w) => w.write_row(&content),
    }
}

#[derive(Parser)]
struct Opts {
    /// File name to contain besv output.
    pub output_path: Option<String>,

    #[clap(short = 'r', long = "rows", default_value_t = 8)]
    /// Number of rows to emit. Range: [0, 255]
    pub row_count: u8,

    #[clap(short = 'c', long = "cols", default_value_t = 8)]
    /// Number of columns to emit. Range: [0, 255]
    pub column_count: u8,
}

/// Entertainment value only
fn number_to_name(value: u8) -> String {
    match value {
        0..=13 => String::from(NUMBER_WORDS[value as usize]),
        14 | 16..=19 => format!("{}teen", number_to_name(value - 10)),
        15 => String::from(NUMBER_WORDS[14]),
        20..=29 => format!("twenty-{}", &number_to_name(value - 20)),
        30..=39 => format!("thirty-{}", &number_to_name(value - 30)),
        40..=49 => format!("fourty-{}", &number_to_name(value - 40)),
        50..=59 => format!("fifty-{}", &number_to_name(value - 50)),
        60..=69 => format!("sixty-{}", &number_to_name(value - 60)),
        70..=79 => format!("seventy-{}", &number_to_name(value - 70)),
        80..=89 => format!("eighty-{}", &number_to_name(value - 80)),
        90..=99 => format!("ninety-{}", &number_to_name(value - 90)),
        100..=255 => format!(
            "{} hundred and {}",
            number_to_name(value / 100),
            number_to_name(value % 100)
        ),
    }
}
