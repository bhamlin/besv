#![allow(unused)]

use besv::BesvReader;
use clap::Parser;
use itertools::Itertools;
use std::{
    fmt::Display,
    fs,
    io::{self, BufRead, Read, Write},
};

type EmptyResult<E> = Result<(), E>;

fn main() -> EmptyResult<io::Error> {
    let opts = Opts::parse();

    let fh = fs::File::open(opts.input_path.unwrap())?;
    let mut besv_reader = BesvReader::new(fh);

    for line in besv_reader.rows() {
        let fields = line
            .iter()
            .map(|field| {
                if field.contains(',') {
                    format!("\"{field}\"")
                } else {
                    field.to_string()
                }
            })
            .collect_vec();
        println!("{}", fields.join(","));
    }

    Ok(())
}

fn input<R, T>(reader: Option<&mut BesvReader<R>>, content: Vec<T>)
where
    R: Read,
    T: Display + ToString,
{
    todo!()
    // match reader {
    //     None => println!("{}", content.iter().join(from_utf8(&[BEL]).unwrap())),
    //     Some(w) => w.write_row(&content),
    // }
}

#[derive(Parser)]
struct Opts {
    #[clap(short = 'i', long = "besv-input")]
    /// File name to contain besv input.
    pub input_path: Option<String>,

    #[clap(short = 'o', long = "csv-output")]
    /// File name to contain csv output.
    pub output_path: Option<String>,
}
